import React, { Component } from "react";
import { connect } from "react-redux";
import { createComment } from "../comments";
import "./AddComment.css";

class AddComment extends Component {
  state = {
    value: ""
  };
  onFormSubmit = e => {
    e.preventDefault();

    const comment = {
      user: "User 3",
      value: this.state.value,
      timestamp: Date.now().toString(),
      timeZoneOffset: new Date().getTimezoneOffset().toString(),
      likes: 0
    };

    this.props.createComment(comment);
    this.setState({ value: "" });
  };
  onFormChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };
  render() {
    return (
      <div>
        <h2>Add new comment</h2>
        <form className="AddComment" onSubmit={this.onFormSubmit}>
          <input
            className="AddComment__value"
            type="text"
            placeholder="Have something to say?"
            autoComplete="off"
            name="value"
            value={this.state.value}
            onChange={this.onFormChange}
          />
        </form>
      </div>
    );
  }
}

export default connect(null, { createComment })(AddComment);
