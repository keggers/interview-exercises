import React from "react";
import { Provider } from "react-redux";
import { mount } from "enzyme";
import App from "./App";
import configureStore from "redux-mock-store";
import Comment from "./Comment";

describe("App", () => {
  it("should show 2 comments when there are 2 comments in the store", () => {
    const initialState = {
      comments: [
        {
          user: "User 1",
          value: "Something user 1 would say...",
          id: 1,
          timestamp: "1502580722572",
          timeZoneOffset: "500",
          likes: 3
        }
      ]
    };

    const mockStore = configureStore();
    const store = mockStore(initialState);
    const wrapper = mount(
      <Provider store={store}>
        <App />
      </Provider>
    );

    expect(wrapper.find(Comment).length).toBe(1);
  });

  it("should show 2 comments when there are 2 comments in the store", () => {
    const initialState = {
      comments: [
        {
          user: "User 1",
          value: "Something user 1 would say...",
          id: 1,
          timestamp: "1502580722572",
          timeZoneOffset: "500",
          likes: 3
        },
        {
          user: "User 2",
          value: "Something user 2 did say...",
          id: 2,
          timestamp: "1502180722530",
          timeZoneOffset: "300",
          likes: 1
        }
      ]
    };
    const mockStore = configureStore();
    const store = mockStore(initialState);
    const wrapper = mount(
      <Provider store={store}>
        <App />
      </Provider>
    );

    expect(wrapper.find(Comment).length).toBe(2);
  });
});
