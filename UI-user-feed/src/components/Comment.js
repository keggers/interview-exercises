import React, { Component } from "react";
import t from "prop-types";
import "./Comment.css";

class Comment extends Component {
  static propTypes = {
    value: t.string.isRequired,
    likes: t.number.isRequired,
    prettyDate: t.string.isRequired,
    onLike: t.func.isRequired
  };
  handleLikeClick = () => {
    this.props.onLike(this.props.id);
  };
  render() {
    const { user, value, prettyDate, likes } = this.props;
    return (
      <div className="Comment">
        <div className="Comment__user">
          {user}
        </div>
        <div className="Comment__value">
          {value}
        </div>
        <button className="Comment__likes" onClick={this.handleLikeClick}>
          {likes} likes
        </button>
        <div className="Comment__timestamp">
          {prettyDate}
        </div>
      </div>
    );
  }
}

export default Comment;
