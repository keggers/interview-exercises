import React, { Component } from "react";
import "./App.css";
import AddComment from "./AddComment";
import Feed from "./Feed";

class App extends Component {
  render() {
    return (
      <div className="App">
        <AddComment />
        <Feed />
      </div>
    );
  }
}

export default App;
