import React, { Component } from "react";
import t from "prop-types";
import { connect } from "react-redux";
import { likeComment } from "../comments";
import { commentsWithPrettyDate } from "../selectors";
import "./Feed.css";
import Comment from "./Comment";

class Feed extends Component {
  static propTypes = {
    comments: t.array.isRequired,
    likeComment: t.func.isRequired
  };
  onLike = (id, numLikes) => {
    this.props.likeComment(id, numLikes);
  };
  render() {
    return (
      <div className="Feed">
        <h2>
          {this.props.comments.length} Comments
        </h2>
        <ul className="Feed__list">
          {this.props.comments.map(({ id, user, value, likes, prettyDate }) => (
            <Comment
              key={id}
              id={id}
              user={user}
              value={value}
              prettyDate={prettyDate}
              likes={likes}
              onLike={this.onLike}
            />
          ))}
        </ul>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  comments: commentsWithPrettyDate(state.comments)
});

const mapDispatchToProps = { likeComment };

export default connect(mapStateToProps, mapDispatchToProps)(Feed);
