const months = [
  "Jan.",
  "Feb.",
  "Mar.",
  "Apr.",
  "May",
  "Jun.",
  "Jul.",
  "Aug.",
  "Sept.",
  "Oct.",
  "Nov.",
  "Dec."
];

/**
 * Converts number to string and pads it with zeroes 
 * @param {Number} num 
 * @return {String}
 */
const padNum = num => num.toString().padStart(2, "0");

/**
 * Returns a user friendly timestamp
 * @param {Number} timestamp - UTC timestamp
 * @example
 * getPrettyTimestamp(1502580722572); 
 * // returns "Aug 12, 2017 at 6:32pm"
 * @return {String}
 */
export function getPrettyTimestamp(timestamp) {
  const dt = new Date(timestamp);
  const yr = dt.getFullYear();
  const mo = months[dt.getMonth()];
  const day = dt.getDate();
  const hrs = dt.getHours();
  const mins = padNum(dt.getMinutes());
  return `${mo} ${day}, ${yr} at ${hrs > 12 ? hrs - 12 : hrs}:${mins}${hrs >= 12 ? "pm" : "am"}`;
}
