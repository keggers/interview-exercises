import { getPrettyTimestamp } from "../utils/date";

const prettyDateCache = {};

export function commentsWithPrettyDate(comments) {
  return comments.map(comment => {
    const timestamp = +comment.timestamp;
    let prettyDate = prettyDateCache[timestamp];
    if (!prettyDate)
      prettyDate = prettyDateCache[timestamp] = getPrettyTimestamp(timestamp);
    return Object.assign({}, comment, { prettyDate });
  });
}
