import reducer, { createComment, likeComment, loadSuccess } from "./comments";

describe("comment reducer", () => {
  test("returns a state object", () => {
    const result = reducer(undefined, { type: "ANYTHING" });
    expect(result).toBeDefined();
  });

  test("adds comments", () => {
    const startState = [];
    const comments = [
      {
        user: "User 1",
        value: "Something user 1 would say...",
        id: 1,
        timestamp: "1502580722572",
        timeZoneOffset: "500",
        likes: 3
      },
      {
        user: "User 2",
        value: "Something user 2 did say...",
        id: 2,
        timestamp: "1502180722530",
        timeZoneOffset: "300",
        likes: 1
      }
    ];
    const expectedState = [...comments];

    const result = reducer(startState, loadSuccess(comments));
    expect(result).toEqual(expectedState);
  });

  test("adds a comment", () => {
    const startState = [
      {
        user: "User 1",
        value: "Something user 1 would say...",
        id: 1,
        timestamp: "1502580722572",
        timeZoneOffset: "300",
        likes: 3
      }
    ];

    const expectedState = [
      {
        user: "User 1",
        value: "Something user 1 would say...",
        id: 1,
        timestamp: "1502580722572",
        timeZoneOffset: "300",
        likes: 3
      },
      {
        user: "User 2",
        value: "Something user 2 did say...",
        id: 2,
        timestamp: "1502180722530",
        timeZoneOffset: "300",
        likes: 0
      }
    ];

    const newComment = {
      user: "User 2",
      value: "Something user 2 did say...",
      timestamp: "1502180722530",
      timeZoneOffset: "300",
      likes: 0
    };

    const result = reducer(startState, createComment(newComment));
    expect(result).toEqual(expectedState);
  });

  test("adds a like", () => {
    const startState = [
      {
        user: "User 1",
        value: "Something user 1 would say...",
        id: 1,
        timestamp: "1502580722572",
        timeZoneOffset: "300",
        likes: 3
      }
    ];

    const expectedState = [
      {
        user: "User 1",
        value: "Something user 1 would say...",
        id: 1,
        timestamp: "1502580722572",
        timeZoneOffset: "300",
        likes: 4
      }
    ];

    const result = reducer(startState, likeComment(1));
    expect(result).toEqual(expectedState);
  });
});
