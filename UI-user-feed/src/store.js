import { combineReducers, createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import comments from "./comments";

const rootReducer = combineReducers({
  comments
});

export default createStore(rootReducer, applyMiddleware(thunk));
