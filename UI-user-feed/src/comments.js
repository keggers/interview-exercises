import initialState from "./initialState";

// action types
const LOAD_SUCCESS = "comments/LOAD_SUCCESS";
const CREATE_COMMENT = "comments/CREATE_COMMENT";
const LIKE_COMMENT = "comments/LIKE_COMMENT";

// comments reducer
export default function reducer(state = initialState.comments, action) {
  switch (action.type) {
    case LOAD_SUCCESS:
      return [...state, ...action.comments];

    case CREATE_COMMENT:
      return [
        ...state,
        Object.assign({}, action.comment, { id: state.length + 1 })
      ];

    case LIKE_COMMENT: {
      const index = state.findIndex(comment => comment.id === action.id);
      const comment = state[index];
      return [
        ...state.slice(0, index),
        Object.assign({}, comment, { likes: comment.likes + 1 }),
        ...state.slice(index + 1)
      ];
    }

    default:
      return state;
  }
}

// actions
export function loadSuccess(comments) {
  return { type: LOAD_SUCCESS, comments };
}

export function loadComments() {
  return async dispatch => {
    const blob = await window.fetch(process.env.PUBLIC_URL + "/data.json");
    const data = await blob.json();
    dispatch(loadSuccess(data.feed));
  };
}

export function createComment(comment) {
  return { type: CREATE_COMMENT, comment };
}

export function likeComment(id) {
  return { type: LIKE_COMMENT, id };
}
