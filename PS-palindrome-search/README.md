# PS-palindrome-search exercise

## Instructions

- Using vanilla javascript, create a function that finds the second longest palindrome in a the string
- First Longest Palindrome could be substring for example "referrer". Here referrer is not a palindrome but 'refer' is. So, the second longest palindrome is 'erre'.
- Output of the function should be as follows
  - when no palindrome exists => 'No Palindrome exists'
  - When there is only one palindrome => 'No Second Palindrome exists'
  - When there is a second palindrome => 'Found Palindrome: [PALINDROME]'
- After you complete the exercise, provide any notes on your code below such as how to run your example

## Candidate Notes:

To run the tests, run `node index.test.js` from the current directory. The test results can be seen in the terminal. More tests can be added to `index.test.js`. 

You can also use the CLI with the command `node cli.js [word]`. Example: `node cli.js referrer` returns `Found Palindrome: erre`.