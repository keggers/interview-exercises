const findSecondLongestPalindrome = require("./index");

/**
 * Super simple equality comparison
 * @param {*} actual 
 * @param {*} expected 
 * @param {String} desc 
 */
function isEqual(actual, expected, desc) {
  if (actual === expected) return console.log(`✓ ${desc}`);
  throw new Error(
    `Test (${desc}) failed. Expected ${actual} to equal ${expected}`
  );
}

/**
 * Super simple testing block
 * @param {String} desc - Description of the test
 * @param {Function} fn - Function with the tests in the body
 */
function test(desc, fn) {
  try {
    console.log(`Running tests for: ${desc}`);
    fn();
    console.log("All tests passed");
  } catch (e) {
    console.log(e.message);
  }
}

test("findSecondLongestPalindrome", () => {
  const actual1 = findSecondLongestPalindrome("referrer");
  const expected1 = "Found Palindrome: erre";
  isEqual(actual1, expected1, "The second longest palindrome should be 'erre'");

  const actual2 = findSecondLongestPalindrome("tartar");
  const expected2 = "No Palindrome exists";
  isEqual(actual2, expected2, "No palindrome should exist for 'tartar'");

  const actual3 = findSecondLongestPalindrome("refer");
  const expected3 = "No Second Palindrome exists";
  isEqual(actual3, expected3, "Only one palindrome should exist for 'refer'");

  // More tests can be added here
});
