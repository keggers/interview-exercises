/**
 * Returns true if str reads the same backwards as forwards, 
 * otherwise returns false
 * @param str {String}
 * @param {Boolean}
 */
function isPalindrome(str) {
  const len = str.length;
  if (!len) return false;
  for (let i = 0; i < len / 2; i++) {
    if (str[i] !== str[len - i - 1]) return false;
  }
  return true;
}

/**
 * Returns the seconds longest palindrome if it exists,
 * otherwise returns a string that explains that either
 * no palindromes exist or that no second palindrome exists.  
 * @param str {String}
 * @return {String}
 */
function findSecondLongestPalindrome(str) {
  let longestPalindrome;
  const strLen = str.length;
  const minLen = 4;
  for (let wordLen = strLen; wordLen >= minLen; wordLen--) {
    const numCombos = strLen - wordLen + 1;
    for (let index = 0; index < numCombos; index++) {
      const word = str.slice(index, wordLen + index);
      if (isPalindrome(word)) {
        if (longestPalindrome) return `Found Palindrome: ${word}`;
        longestPalindrome = word;
      }
    }
  }
  return longestPalindrome
    ? "No Second Palindrome exists"
    : "No Palindrome exists";
}

module.exports = findSecondLongestPalindrome;
